import logo from './logo.svg';
import './App.css';

import AboutUsPage from './Pages/AboutUs.Page';
import BMIcalPage from './Pages/page';
import Header from './coponents/Header';
import { Route, Routes } from 'react-router-dom';
import LuckyNumberPage from './Pages/LuckyNumberPage';


function App() {

  return (
    <div className="App">
      <Header />
    <Routes>

             <Route path= "about" element= {
               <AboutUsPage />
              }/>


              <Route path="/" element={
                 <BMIcalPage />
              } />


              <Route path="/LuckyNumber" element ={
                <LuckyNumberPage/>
              }/>
     </Routes>
    </div>
  );
}

export default App;