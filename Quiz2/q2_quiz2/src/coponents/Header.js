import { Link } from "react-router-dom"
function Header() {
    return (

        <div className="header">
            <div className="temp-header">
                <span>
                    ยินดีต้อนรับสู่เว็บทายเลข ว่าเป็นจำนวนคู่หรือคี่ หรือ 0 :
                </span>
                
                <Link to="/">
                    <span>
                        ผู้จัดทำ
                    </span>
                </Link>
                
                <Link to="/Luckynumber">
                    <span>
                        สุ่มเลข
                    </span>
                </Link>

               
            </div>
            <hr />

        </div >

    );

}

export default Header;