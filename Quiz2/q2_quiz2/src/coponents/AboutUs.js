
function AboutUs (props) {

    
    return (

       <div>
           <h2> จัดทำโดย: { props.name}</h2>
           <h3> ติดต่อได้ทาง FB {props.address}</h3>
           <h3> อยู่ที่นครศรีธรรมราช {props.provinc}</h3>
       </div>
    
    );

}

export default AboutUs;